package com.sample.demoproject.page_stepactions;

import com.sample.demoproject.page_objects.FindMyCard;
import net.thucydides.core.annotations.Step;

public class FindMyCardStepActions {

    FindMyCard findMyCardPage;

    @Step
    public void openApplication() {
        findMyCardPage.open();
    }

    @Step
    public void seeFindMyCardPage() {
        findMyCardPage.shouldBeDisplayed();
    }

}
