package com.sample.demoproject.test_runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = {"classpath:features"}
        , glue = {"com.sample.demoproject.step_definitions"}
)

public class SerenityCucumberRunner {
}
