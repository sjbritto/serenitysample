package com.sample.demoproject.page_objects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;

@DefaultUrl("/creditcards/secure/find-my-card.jsf")
public class FindMyCard extends PageObject {

    @WhenPageOpens
    public void onOpen() {
        $("//div").waitUntilVisible();
    }

}
