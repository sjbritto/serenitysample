package com.sample.demoproject.step_definitions;

import com.sample.demoproject.page_stepactions.FindMyCardStepActions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class FindMyCardStepDefinitions {

    @Steps
    FindMyCardStepActions i;

    @Given("I open the demo application")
    public void i_open_demo_application_in_browser() {
        i.openApplication();
    }

    @Given("{string} launches browser and navigates to Fill in your details page")
    public void launches_browser_and_navigates_to_fill_in_your_details_page(String user) {
        i.seeFindMyCardPage();
    }

}
