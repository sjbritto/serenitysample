Feature: Unhappy path to check that error messages appear on page

  Background: Successfully launch the Capital One - Fill in your details page
    Given I open the demo application

#  Rule: User should be able to see error messages on - Fill in your details page

    Scenario: User sees error message when mandatory fields are left blank
      Given "Mark" launches browser and navigates to Fill in your details page
#      When user completes the form with missing data the mandatory fields
#      And user submits the application
#      Then user will see error messages on the fields